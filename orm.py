# %%
from sqlalchemy import create_engine, Column, String, Integer
from sqlalchemy.orm import declarative_base, sessionmaker

Base = declarative_base()


class Log(Base):
    __tablename__ = 'logs'

    INFO = 30
    ERROR = 50

    id = Column(Integer, primary_key=True)
    level = Column(Integer)
    message = Column(String)

    def __repr__(self):
        name = self.__class__.__name__
        return f'{name}(id={self.id!r}, level={self.level!r}, message={self.message!r})'


# engine = create_engine('sqlite:////tmp/logs.db')
engine = create_engine('sqlite:///:memory:')
Base.metadata.create_all(engine)

Session = sessionmaker(bind=engine)

session = Session()

log = Log(level=Log.INFO, message='server starting')
session.add(log)
log = Log(level=Log.ERROR, message='bad credentials')
session.add(log)

# session.dirty

for log in session.query(Log).filter_by(level=Log.INFO):
    print(log)

session.commit()

# %%
