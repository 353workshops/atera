"""Object Oriented Python

Classes and instances.

- Class defines behavior
- Instance knows only two things
    - attributes
    - it's class

"""


# %%
import sqlite3

# Exercise: Write a "Bid" class with the following attributes:
# - auction
# - bidder
# - price
# Have "bids" method return list of Bid instances instead of dicts

# %%
class Bid:
    def __init__(self, auction, bidder, price):
        self.auction = auction
        self.bidder = bidder
        self.price = price

    def __repr__(self):
        # A way to create such instance
        name = self.__class__.__name__
        return f'{name}({self.auction!r}, {self.bidder!r}, {self.price!r})'

# %%

class BidsDB:
    version = '1.2.3'  # class attribute, shared

    # __init__ is method, called "dunder init"
    # __init__ is a special method
    # self is the current instance, filled by Python
    def __init__(self, db_file):
        self.db_file = db_file
        self.conn = sqlite3.connect(self.db_file)
        # access columns by name
        self.conn.row_factory = sqlite3.Row

    def bids(self, auction):
        # BUG: SQL Injection, never use f-string to create SQL
        # sql = f'SELECT * FROM bids WHERE auction = {auction}'
        sql = 'SELECT * FROM bids WHERE auction = ?'
        cur = self.conn.execute(sql, (auction, ))
        return [Bid(**row) for row in cur]
        #return [dict(row) for row in cur]

    def close(self):
        self.conn.close()

db = BidsDB('data/auctions.db')  # db is instance of Bids
print(isinstance(db, BidsDB))

# %%
auction = 5
bids = db.bids(5)
print(bids)
bid = bids[0]
print(bid.price)

# %%
print('version:', db.version)
print('db_file:', db.db_file)
print('class:', db.__class__)
print('attributes:', db.__dict__)

# %%
# Without a class, user will need to pass conn every time.
def auction_bids(conn, auction):
    ...



# %%
print(BidsDB.version)
# %%

db2 = BidsDB('data/auctions_2022.db')
print(db.db_file)
# %%
def find_attribute(obj, attr):
    """Emulate built-in getattr"""
    if attr in obj.__dict__:
        print(f'found {attr} in instance')
        return obj.__dict__[attr]
    if attr in obj.__class__.__dict__:
        print(f'found {attr} in class')
        return obj.__class__.__dict__[attr]

    raise AttributeError(attr)

find_attribute(db, 'db_file')
find_attribute(db, 'version')

# AttributeError usually means one of two things:
# - typo
# - The object your working is not what you think
# %%
a, b = 1, 2
print(a < b)
# The above is mapped to:
print(a.__lt__(b))
# %%

x, y = '1', 1
print(f'x={x}, y={y}')  # str
print(f'x={x!r}, y={y!r}')  # repr


# %%
print('Back 10:55')
# %%
