"""
Find the sum of all the multiples of 
3 or 5 below 1000.

233168
"""

# Solution stages
# Get (HTTP request, read a file ...)
# Parse (extract from JSON)
# Analyze (computaton)
# Output (print)

# Get
# Numbers form 1 to 999
total = 0
for n in range(1_000):
    # Parse: NOP
    # If number divides by 3 or 5, add to total
    if n % 3 == 0 or n % 5 == 0:
        # total = total + n
        total += n
# Output: print
print(total)
