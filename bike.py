# %% Load Data
import pandas as pd

df = pd.read_csv('Austin_MetroBike_Trips.csv.bz2', parse_dates=['Checkout Datetime'])

# %% Remove rows where `Bicyle ID` is not an integer and convert the column to integer. How many rows are left?
mask = df['Bicycle ID'].str.match(r'^\d+$')
df = df[mask]
df['Bicycle ID'] = df['Bicycle ID'].astype('int64')
print(len(df))

# %% What the day of the week which has highest median number of trips?
df['date'] = df['Checkout Datetime'].dt.date
df['wday'] = df['Checkout Datetime'].dt.day_name()
daily_count = df.groupby(['date', 'wday'], as_index=False).count()
wday_max = daily_count.groupby('wday')['Trip ID'].max()
print(wday_max.idxmax())

# %% What is the most common return kiosk name? (You'll need to join with the kiosk data)

# Need to remove rows without Return Kiosk ID
df = df[~pd.isnull(df['Return Kiosk ID'])]
loc_df = pd.read_csv('Austin_MetroBike_Kiosk_Locations.csv')
kiosk_df = pd.merge(df, loc_df, left_on='Return Kiosk ID', right_on='Kiosk ID')
print(kiosk_df['Kiosk Name'].value_counts().idxmax())
