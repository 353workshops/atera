print(1 + 1)

# REPL: Read, Eval, Print, Loop

# %%
1 + 1
# %%
2**1000
# %%
7 / 3
# %%
7 // 3
# %%
0.1 + 0.1 + 0.1
# %%
1.1 * 1.1
# %%
from decimal import Decimal
Decimal('1.1') * Decimal('1.1')
# %%
2+7j
# %%
n = float('nan')
# %%
n == n
# %%
import math
math.isnan(n)
# %%

None
# %%
round(0.5)
# %%
round(1.5)
# %%
round(2.5)
# %%
# Bankers rounding: Even down, odd up


# Control Flow
n = 10

if n < 20:
    print('n is small')
# %%
if n < 10:
    print('n is small')
else:
    print('n is big')
# %%
if n < 2:
    print('n is small')
elif n < 7:
    print('n is medium')
else:
    print('n is big')
# %%
size = 'big' if n > 10 else 'small'
print(size)
# %%
n = 'a'
print(n)
# %%
print('hi')

# %%
# Collatz step:
# if n is even: n / 2
# if n is odd: n * 3 + 1

def collatz(n):
    if n % 2: # != 0:  # odd
        return n * 3 + 1
    
    return n // 2  # even

print(collatz(3))  # 10
print(collatz(8))  # 4

# %% Truth in Python
# In Python, everything is True. Except
# - 0 numbers (0, 0.0, 0+0j ...)
# - empty collection ([], '', ...)
# - None
# - False

cart = ['avocado', 'bread']
#if len(cart) > 0:
if cart:
    print('user has items in cart')


# %%
greeting = 'Hello'
greeting = "Hello"
greeting = '''Hello'''
greeting = """Hello"""

poem = '''
Mary had a little lamb
little lamb ...
'''
print(poem)

# %%
csv_file = r"c:\to\new\report.csv"
print(csv_file)
# raw strings: use in windows path & regular expressions

# %%
name, age = 'Daffy', 84
f'{name} is {age} years old'

# %%
'hello' + ' there'
# %%


print(f'{name} is {age*7} in dogs years old')
# %%
a, b  = 1, '1'
print(f'a={a}, b={b}')
print(f'a == b: {a == b}')
# %%

print(f'a={a!r}, b={b!r}')
# Two ways to represent as string
# - str: for users
# - repr: for developers

# %%
print(f'{a=}, {b=}')
# %%
import math
print(f'pi = {math.pi:.2f}')

# %%
values = [
    10,
    212,
    345,
    1090,
]
for val in values:
    print(f'{val: 5d}')

# %%
n = 1_000_000_000
print(f'n = {n:,}')

# %%
a = 1,000 # a is a tuple
print(a + 1)

# %%
print(10)  # decimal
print(0x10) # hexa
print(0o10) # octa
print(0b10) # binary

# %%
# cart = ('carrot', 'lettus') # tuple
cart = 'carrot', 'lettus' # tuple
print(cart)

# %%
# Mutation vs re-binding
names = ['Daffy', 'Bugs', 'Taz']
names[0] = 'daffy'  # mutation
names.append('elmer') # mutation
print(names)
old = names

names = ['mickey', 'donald'] # re-binding
print(names)

# %%
print(old)

# %%
greeting = 'שלום'
print(greeting)
print(greeting[0])
# %%
type(greeting)
# %%
data = greeting.encode('utf-8')
type(data)

# %%
print(data.decode('utf-8'))

# %%
for c in greeting:
    print(c)
# %%
for i in range(5):
    print(i)
# %%

for i in range(7, 10):
    print(i)

# %%
a = 1
while a < 4:
    print(a)
    a += 1
# %%
names = ['volves', 'fish', 'crows']
groups = ['pack', 'school', 'murder']

for name in names:
    print(name)
# %%

for i, name in enumerate(names):
    print(f'{name} at {i}')
# %%
# for v in enumerate(names):
#    i, name = v
for i, name in enumerate(names):
    group = groups[i]
    print(f'I saw a {group} of {name}')

# %%
for name, group in zip(names, groups):
    print(f'I saw a {group} of {name}')

# %%
values = [1, 2]
a, b = values  # unpacking
print(a, b)
# %%
# Back 12:35

# symbol -> price
stocks = {
    'MSFT': 373.07,
    'AAPL': 190.64,
}
print(stocks['AAPL'])
# %%
# print(stocks['NVDA'])  # key error
print(stocks.get('NVDA'))  # None


# %%
for symbol in stocks:
    print(symbol)

# %%
for symbol in stocks:
    value = stocks[symbol]
    print(symbol, '->', value)


# %%
print(stocks)
# %%
for symbol, value in stocks.items():
    print(symbol, '->', value)
# %%
stocks['NVDA'] = 499.44
print(stocks)
# %%
del stocks['AAPL']
stocks.pop('AAPL', None)  # Won't fail if AAPl is not there

# %% by date

daily = {
    ('NVDA', '2023-11-2'): 499.4,
}

# %%
options = {
    'verbose',
    'port',
}
options.add('port')
print(options)
# %%
'port' in options

# %%
user_options = {'port', 'output'}
print(options & user_options)  # disjunction
print(options | user_options)  # conjunction
print(options - user_options)
# %%
print(type([]))
print(type(()))
print(type({}))

# %%
set()

# %%


# %% List Comprehension
# sum of all squares from 1 to 10
sum([i*i for i in range(1, 11)])
# SELECT SUM(i*i) FROM range(1, 11)
# %%

# sum of all squares from 1 to 10, only odd
sum([i*i for i in range(1, 11) if i%2==1])
# SELECT SUM(i*i) FROM range(1, 11) WHERE i%2==1

# %%
[i * i for i in range(5)]

# %% Euler 1
sum([i for i in range(1000) if i % 5 == 0 or i % 3 == 0])

# %%
names = ['Daffy', 'Taz', 'Elmer']
[n.lower() for n in names if len(n) > 3]

# %%
d1 = {'a': 1, 'b': 2}
d2 = {'b': 3, 'c': 4}
d1 | d2  # Python 3.11+
# %%
d = {}
d.update(d1)
d.update(d2)
print(d)

# %%

book = 'The Colour of Magic'
print(book[:3])  # half-open
print(book[0:3])
print(book[3:])
print(book[3:10:2])
print(book[-1])
# print(book[len(book):-1:-1])
print(book[::-1])


# %%
name = 'Daffy_Duck'
i = name.find('_')
first = name[:i]
last = name[i+1:]
print(f'first={first}, last={last}')

# %%
name = 'Daffy_Duck'
first, last = name.split('_')
print(f'first={first}, last={last}')


# %%
heystack = 'How are you?'
needle = 'hi'
# if heystack.find(needle): # BUG: -1 is True
if needle in heystack:
    print('found')
else:
    print('no found')
# %%
def sub(a, b):
    """Returns a minus b.
    
    >>> sub(1, 7)
    -6
    """
    return a - b

sub(1, 7)
# %%
args = (1, 7)
# sub(args[0], args[1])
sub(*args)

# * in function call, unpacks a sequence to positional arguments

# %%
def vargs(*values):
    print(f'{values=}')

vargs()
vargs(1,2,3)

# * in function definition will pack positional arguments to a tuple
# %%
sub(a=1, b=7)

# %%

sub(b=7, a=1)
# %%

sub(1, b=7)


# %%
# sub(b=7, 1)  # SyntaxError

# %%
def plot(xs, ys, color, line_width=1, opacity=0.8):
    ...


def monthly_plot(month, **kw):
    data = ... # Get from datbase
    plot(data['date'], data['income'], **kw)

# %%
args = {'a': 1, 'b': 7}
sub(**args)
# sub(a=1, b=7)
# ** in function call unpacks a mapping to keyword arguments
# %%
def kwargs(**kw):
    print(f'{kw=}')

kwargs()
kwargs(x=1, z=3)
# ** in function definition will pack keyword arguments to a dict
# %%
def div(a, b, verbose=False):
    if verbose:
        print(f'div {a} {b}')
    return a / b

div(7, 3, verbose=True)

# %%
