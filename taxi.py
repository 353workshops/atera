# %% Load Data
# %pip install --upgrade pandas
# %pip install pyarrow

import pandas as pd

file_name = 'data/yellow_tripdata_2022-03.parquet'
df = pd.read_parquet(file_name)
# df = pd.read_parquet(file_name, dtype_backend='pyarrow')
# Read about pd.read_sql
df


# %%
df.columns
# %%
len(df)
# %%
df.head(5)
# %%
df.sample(5)
# %%
# df.index = df['tpep_pickup_datetime']
# df.index = ['a', 'b', 'c']

col = df['tpep_pickup_datetime']
col
# %%
df.dtypes
# passenger_count is float64 since we have missing values
# What is missing?
# - NaN
# - None
# - NaT


# %%
col.describe()
# %%
df['trip_distance'].describe()

# %%
# python -m pip install matplotlib
df['tip_amount'].plot.box()
# %%
s = pd.Series([1,2,3,4,5])
s
# %%
s > 2
# %%
s[s > 2]  # boolean indexing

# %%
s[(s>1) & (s<4)]
# & = and
# | = or
# ~ = not
# %%
mask = pd.isnull(df['passenger_count'])
df = df[~mask]
df
# %%
df[100:110]
# %%
df[['VendorID', 'passenger_count']]
# %%

df[['VendorID', 'passenger_count']][200:205]
# %%
df['duration'] = df['tpep_dropoff_datetime'] - df['tpep_pickup_datetime']
df['duration'].describe()
# %%
mask = df['total_amount'] < df['total_amount'].quantile(0.75)
df[mask]
# %%
start = df['tpep_pickup_datetime']
mask = (start.dt.year == 2022) & (start.dt.month == 3)
# mask = (start >= '2022-03') & (stat < '2022-04')
df = df[mask]
df

# df = df[(df['tpep_pickup_datetime'] >= '2022-03) & (...)]

# For datetime (timestamp) methods use .dt
# For string (text) methods use .str
# %%
# Exercise: How many rides with more than one passenger
# Between 5pm and 7pm?
# Use tpep_pickup_datetime
mask = (
    (df['tpep_pickup_datetime'].dt.hour >= 17) &
    (df['tpep_pickup_datetime'].dt.hour <= 19) &
    (df['passenger_count'] > 1)
)
len(df[mask])
# %% How many rides in first week (1-7)
mask = (
    (df['tpep_pickup_datetime'] >= '2022-03-01') &
    (df['tpep_pickup_datetime'] <= '2022-03-07')
)
len(df[mask])

# %%
def vendor_name(vendor_id):
    if vendor_id == 1:
        return 'Creative'
    if vendor_id == 2:
        return 'VeriFone'

    return ''

df['vendor_name'] = df['VendorID'].apply(vendor_name)
df['vendor_name'].sample(10)
# vendor_name dtype is "object" (most of the time str)
# Use pyarrow for compact strings (less memory)
df['vendor_name'] = df['vendor_name'].astype('string[pyarrow]')

# %% How many rides per vendor
df['vendor_name'].value_counts()

# %%
# column name to group by
df.groupby('vendor_name')['tip_amount'].count()
# %% How many rides for each hour of the day?
# Values to group by
df.groupby(df['tpep_pickup_datetime'].dt.date)['vendor_name'].count()


# %%
# flow style
(
    df.groupby(df['tpep_pickup_datetime'].dt.date)
    # Random column to count
    ['vendor_name']
    .count()
    .plot.bar()
)

# %% What is the median tip_amount per passenger_count?
df.groupby('passenger_count')['tip_amount'].median()
# %%

df.groupby('passenger_count')['tip_amount'].agg(['count', 'median'])

# %% What is the median number of rides per hour in the day of the week?

# Auxiliary columns
df['date'] = df['tpep_pickup_datetime'].dt.round('D')
df['hour'] = df['tpep_pickup_datetime'].dt.hour


# %%
daily = (
    df.groupby(['date', 'hour'], as_index=False)
     .count()
)
ax = (
    daily.groupby('hour')
    ['VendorID']  # need only one column
    .median()
    .plot.bar(
        rot=0,
        title='Median Hourly Rides',
        color='#FF176B',
    )
)
ax.set_ylabel('number of rides')
ax.set_xlabel('hour of day')



# %%

import matplotlib.pyplot as plt

plt.style.available
plt.style.use('ggplot')

# %%
# python -m pip install plotly
import plotly.express as px

hourly = (
    daily.groupby('hour')
    ['VendorID']  # need only one column
    .median()
)

px.bar(hourly)
# %% Draw a line chart of cumulative revenue per day
# total_amount, df.cumsum
# Label the axies and give a title

# s = pd.Series([1,2,3])
# s.cumsum()

df['date'] = df['tpep_pickup_datetime'].dt.round('D')
daily_revenue = df.groupby('date')['total_amount'].sum()
ax = daily_revenue.cumsum().plot(title='Daily Revenue (cumulative)')
ax.set_ylabel('cumulative revenue');

# TIP: Add ; at end of last code line when displaying
# chars
# %%
