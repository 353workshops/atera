import logging

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(filename)s:%(lineno)d %(levelname)s - %(message)s',
)

logging.info('pipeline started')

db_name = 'db1.atera.com'
logging.error('cannot connect to database %s', db_name)


# If logging doesn't help you during development, it won't help you in production.
# - Bill Kennedy