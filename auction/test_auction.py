import auction
import pytest
from os import environ


def test_second_highest():
    values = [1, 3, 2]
    out = auction.second_highest(values)
    assert 2 == out, 'second_highest'


sh_cases = [
    ([1, 3, 2], 2),
    ([0, 0, 0], 0),
    ([3, 4, 1, 2], 3),
]

# @ is a decorator
@pytest.mark.parametrize('values, expected', sh_cases)
def test_second_highest_many(values, expected):
    out = auction.second_highest(values)
    assert expected == out


# On Jenkins use BUILD_NUMBER
in_ci = 'CI' in environ

@pytest.mark.skipif(not in_ci, reason='only in CI')
def test_in_ci():
    pass


def test_size():
    # "with" is a context manager
    with pytest.raises(ValueError):
        auction.second_highest([])

    with pytest.raises(ValueError):
        auction.second_highest([1])

# pytest looks for files <name>_test.py or test_<name>.py
# then look for every function starting with "test" inside and runs it.

# When fixing bug
# - write failing test
# - fix code until test passes

# What assert does
def ensure(condition, message=''):
    """emulate `assert` keyword"""
    if not condition:
        raise AssertionError(message)

# From command line:
# python -m pytest -v

# VScode
# Python: Configure Tests (select pytest)

# PyCharm
# Settings/Tools/Python Integrated Tools/Testing

# %%