def second_highest(values):
    """Return second highest number from values."""
    if len(values) < 2:
        raise ValueError(f'only {len(values)} values')

    first, second = values[0], values[1]
    if second > first:
        first, second = second, first

    for value in values[2:]:
        if value > first:
            first, second = value, first
        elif value > second:
            second = value

    return second
