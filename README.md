# Python Workshop @ Atera

Miki Tebeka
📬 [miki@353solutions.com](mailto:miki@353solutions.com), 𝕏 [@tebeka](https://twitter.com/tebeka), 👨 [mikitebeka](https://www.linkedin.com/in/mikitebeka/), ✒️[blog](https://www.ardanlabs.com/blog/)

#### Shameless Plugs

- [LinkedIn Learning Classes](https://www.linkedin.com/learning/instructors/miki-tebeka)
- [Python Brain Teasers](https://pragprog.com/titles/d-pybrain/python-brain-teasers/)
- [Pandas Brain Teasers](https://pragprog.com/titles/d-pandas/pandas-brain-teasers/)

[Syllabus](data/syllabus.pdf)

## Warm-up Exercises

- [Solve Euler #2](https://projecteuler.net/problem=2)
    - Solution: `4613732`
    - [Miki's solution](euler2.py)
- [Solve Euler #8](https://projecteuler.net/problem=8)
    - Solution: `23514624000`
    - [Miki's solution](euler8.py)
- Write a `VM` class that will:
    - Get an `id` and start with `running` state
    - Have a `shutdown` method that will set the state to `stopped`
    - [Miki's solution](vm.py)
- Load [Taxi data](https://d37ci6vzurychx.cloudfront.net/trip-data/yellow_tripdata_2022-03.parquet) to a pandas DataFrame and find out which `VendorID` has the highest median `trip_distance`.
    - Answer: `5`
    - [Miki's solution](taxi_distance.py)

## Day 1: Functions, Errors & Data Types

### Agenda

- Control flow
- Defining & calling functions
    - Documentation
    - `*args` & `**kw`
    - Variable scope
- Numbers
- Strings
- Lists & tuples
    - Slicing
    - List comprehension
- Dictionaries & sets


### Code

- [euler1.py](euler1.py) - Solving Euler problem 1
- [day1.py](day1.py) - Code from class

### Exercise

Print out the distribution (as percentage) of HTTP status code in `data/http.log.gz`.
A log line looks like:
`uplherc.upl.com - - [01/Aug/1995:00:00:07 -0400] "GET / HTTP/1.0" 304 0`, the status code is the field before last (`304` in this case.)

You should print something like:
```
200:  91.142
304:   6.551
302:   1.495
404:   0.809
403:   0.003
```

Hits: The `gzip` module, `str.split`


### Links

- [The Python Tutorial](https://docs.python.org/3/tutorial/index.html)
- [Euler Project](https://projecteuler.net/)
- [Animal group names](https://grammar.yourdictionary.com/word-lists/list-of-names-for-groups-of-animals.html)
- [Google's Colab](https://colab.research.google.com/?utm_source=scs-index) - Notebooks on the cloud
- [Plain Text](https://www.youtube.com/watch?v=4mRxIgu9R70) - Learn about Unicode
- [Built-in Super Heros](https://www.youtube.com/watch?v=lyDLAutA88s) - Working with data
- [Pythontutor](https://pythontutor.com/) - Visualize execution
- [built-in functions](https://docs.python.org/3/library/functions.html)
- String Formatting
    - [f strings](https://docs.python.org/3/reference/lexical_analysis.html#f-strings)
    - [str.format](https://docs.python.org/3/library/string.html#formatstrings)
    - [String % Formatting](https://docs.python.org/3/library/stdtypes.html#string-formatting)
- [Floating Point Zine](https://wizardzines.com/comics/floating-point/) by Julia Evans
- [What Every Computer Scientist Should Know About Floating-Point Arithmetic](http://docs.oracle.com/cd/E19957-01/806-3568/ncg_goldberg.html)


### Data & Other

- [http.log.gz](data/http.log.gz)

---

## Day 2: OO & Project Engineering

### Agenda

- Object oriented Python
    - Classes, instances, methods & attribute access
- Dependency management
    - Picking & installing 3 party libraries
    - Virtual environments
- Testing with pytest
- Effective Logging

### Code

- [log_demo.py](log_demo.py) - Logging demo
- [test_auction.py](auction/test_auction.py) - Testing with `pytest`
    - [auction.py](auction/auction.py) - Tested code
- [Dependency management](installing.md)
- [oo.py](oo.py) - Object Oriented Python

### Exercise

Create a `Stocks` class that will answer queries on a stocks SQLite3 database (see [stocks.db](data/stocks.db)).
The class should be initialized with the database file name and have three methods:
- `close(self)` that will close the underlying connection.
- `symbols(self)` that will return the list of symbols in the database.
- `records(self, symbol, start_date, end_date)` that will return rows as dicts for the specific symbol between these dates.

Example:

```python
>>> db = Stocks('stocks.db')
>>> db.symbols()
['NDVA', 'MSFT', 'GOOG', 'AAPL', 'TSLA']
>>> db.records('NVDA', '2023-06-01', '2023-06-07')
[
    {
        'symbol': 'NVDA',
        'date': '2023-06-01T00:00:00',
        'open': 384.890015,
        'high': 400.5,
        'low': 383.399994,
        'close': 397.700012,
        'adj_close': 397.626129,
        'volume': 63587300
    },
    ...
]
```

The `stocks` table schema is:

```sql
CREATE TABLE IF NOT EXISTS "stocks" (
  "symbol" TEXT,
  "date" TIMESTAMP,
  "open" REAL,
  "high" REAL,
  "low" REAL,
  "close" REAL,
  "adj_close" REAL,
  "volume" INTEGER
);
```

Write tests to check your code.


### Links

- [Netflix Prize](https://en.wikipedia.org/wiki/Netflix_Prize)
- OO
    - [Classes tutorial](https://docs.python.org/3/tutorial/classes.html)
    - [Special method names](http://docs.python.org/reference/datamodel.html#special-method-names)
    - [Super Considered Super](https://www.youtube.com/watch?v=EiOglTERPEo)
    - [Python's Class Development Kit](https://www.youtube.com/watch?v=HTLu2DFOdTg)
- Package managers
    - [Python Environments Comic](https://xkcd.com/1987/)
    - [pip](https://pip.pypa.io/en/stable/)
    - [Our Software Dependency Problem](https://research.swtch.com/deps)
    - [pyproject.toml](https://pip.pypa.io/en/stable/reference/build-system/pyproject-toml/)
    - [conda](https://docs.conda.io/en/latest/)
    - [poetry](https://python-poetry.org/)
    - [pipenv](https://pipenv.pypa.io/en/latest/)
    - [pyenv](https://github.com/pyenv/pyenv) - Multiple Python versions
- Testing
    - [pytest](https://docs.pytest.org/) - *The* test suite
    - [hypothesis](https://hypothesis.readthedocs.io/) - Fuzz testing
    - [unittest.mock](https://docs.python.org/3/library/unittest.mock.html) - Mocking
    - [How SQLite is Tested](https://www.sqlite.org/testing.html)
- [ruff linter](https://docs.astral.sh/ruff/)
- Logging
    - [Logging HOWTO](https://docs.python.org/3/howto/logging.html#logging-basic-tutorial)
    - [Logging in Python](https://realpython.com/python-logging/)

### Data

- [auctions.db](data/auctions.db)

---

## Day 3: Data Analysis with Pandas

### Agenda

- A look at SQLAlchemy
- Data ingestion
    - CSV/Excel
    - Databases (and when to use them)
        - A look at the SnowFlake driver
- Data selection
- Running calculations
- Cleaning data
    - Filling missing value
    - Finding outliers
- Split-apply-combine (group by)
- Visualization

### Code

- [pd_sql_demo.py](pd_sql_demo.py) - Using Pandas with databases
- [taxi.py](taxi.py) - Pandas demo
- [orm.py](orm.py) - Using SQLAlchemy


### Exercise

Download [Austin bike trips data][td] and [Austin kiosk location data][kd]. In both, click on `Export` and pick `CSV` format.

- Load trip data to a data frame
- Remove rows where `Bicyle ID` is not an integer and convert the column to integer. How many rows are left?
    - Solution: 2015461
- What the day of the week which has highest median number of trips?
    - Thursday
- What is the most common return kiosk name? (You'll need to join with the kiosk data)
    - 21st & Speedway @PCL

[Miki's solution](bike.py)

[td]: https://data.austintexas.gov/Transportation-and-Mobility/Austin-MetroBike-Trips/tyfh-5r8s
[kd]: https://data.austintexas.gov/Transportation-and-Mobility/Austin-MetroBike-Kiosk-Locations/qd73-bsdg/data

### Links

- [Pandas User Guide](https://pandas.pydata.org/docs/user_guide/index.html)
    - [I/O tools](https://pandas.pydata.org/docs/user_guide/io.html)
    - [Indexing & Selecting Data](https://pandas.pydata.org/docs/user_guide/indexing.html)
    - [Working with Missing Data](https://pandas.pydata.org/docs/user_guide/missing_data.html)
    - [Group by: split, apply, combine](https://pandas.pydata.org/docs/user_guide/groupby.html)
    - [Chart Visualization](https://pandas.pydata.org/docs/user_guide/visualization.html)
- Alternate data frames
    - [dask](https://docs.dask.org/) - Run in parallel
    - [polars](https://pola.rs/) - Fast computation
- [Snowflake + Python](https://docs.snowflake.com/en/developer-guide/python-connector/python-connector-connect)
- [Tidy Data](https://vita.had.co.nz/papers/tidy-data.pdf)
- [PyArrow](https://arrow.apache.org/docs/python/index.html) - For reading parquet
- [Pandas + MongoDB](https://www.mongodb.com/developer/languages/python/pymongoarrow-and-data-analysis/)
    - Use to read, then `df.to_sql` to save to database
- SQL -> Pandas
    - [SQL to Pandas converter](https://sql2pandas.pythonanywhere.com/)
        - [Cheatsheet](https://sql2pandas.pythonanywhere.com/download-files/sql-to-pandas-cheat-sheet)
    - [Pandas cheatsheet for SQL people](https://www.kaggle.com/code/adilaliyev/pandas-cheatsheet-for-sql-people)
- [Python Data Science Handbook](https://jakevdp.github.io/PythonDataScienceHandbook/)
- Visualization Libraries
    - [matplotlib](https://matplotlib.org/)
    - [plotly](https://plotly.com/python/) & [dash](https://dash.plotly.com/)
    - [seaborn](https://seaborn.pydata.org/)
    - [holoviz](https://holoviz.org/)
    - [streamlit](https://streamlit.io/)
    - [bokeh](https://docs.bokeh.org/)
    - [altair](https://altair-viz.github.io/)
- [SQLAlchemy](https://www.sqlalchemy.org/)

### Data

- [yellow_tripdata_2022-03.parquet](https://d37ci6vzurychx.cloudfront.net/trip-data/yellow_tripdata_2022-03.parquet)
    - On colab/notebook: `!curl -LO https://d37ci6vzurychx.cloudfront.net/trip-data/yellow_tripdata_2022-03.parquet`
    - [Schema](https://www.nyc.gov/assets/tlc/downloads/pdf/data_dictionary_trip_records_yellow.pdf)
