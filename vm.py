class VM:
    def __init__(self, id):
        self.id = id
        self.state = 'running'

    def shutdown(self):
        self.state = 'stopped'


vm = VM('fa1a937')
print(f'id: {vm.id}, state: {vm.state}')
vm.shutdown()
print(f'id: {vm.id}, state: {vm.state}')
