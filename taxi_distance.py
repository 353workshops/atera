import pandas as pd

df = pd.read_parquet('yellow_tripdata_2022-03.parquet')
mean_distance = df.groupby('VendorID')['trip_distance'].median()
print(mean_distance.idxmax())
