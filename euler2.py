# By considering the terms in the Fibonacci sequence whose values do not exceed four
# million, find the sum of the even-valued terms.
# Answer: 4613732
# (https://projecteuler.net/problem=2)

total = 0
a, b = 1, 2
while a <= 4_000_000:
    if a % 2 == 0:
        total += a
    a, b = b, a + b

print(total)
