# Installing 3rd Party Packages

## Package Selection
- Ask around
- Look at PyPI
- See GitHub stats (stars, commits ...)

Run a POC with a couple of packages then choose.

## Installing

Tools:
- venv (built-in)
- pip
- virtualenv
- conda
- poetry
- pipenv
- ...

### Virtual Environments

Create one virtual environment per project.
WARNING: Never use the system Python interpreter.

```
$ python -m venv .venv
```

Don't forget to add .venv to .gitignore

In your IDE, select the Python interpreter from the virtual environment.
In VSCode, CTRL-SHIFT-P + "Python: Select Interpreter".

In the shell, pick the Python from the virtual environment.

```
$ source .venv/bin/activate
```

It's a good idea to update pip once you created virtual environment.

```
$ ./.venv/bin/python -m pip install --upgrade pip
```

### Installing

```
$ python -m pip install pandas==2.1.3
```

Create a file with all requirement per project.

*requirements.txt
```
pandas~=2.1
```

Install from file:
```
$ python -m pip install -r requirements.txt
```

You should have another file for development requirements.

*dev-requirements.txt*
```
# Include production requirements
-i requirements.txt

pytest ~= 7.2
```

To install:
```
$ python -m pip install -r dev-requirements.txt
```


## Updating Packages

### When

Whenever there's a minor patch, mostly if it's a security patch.
When you need a new feature.
When a version you're using is deprecated.


### How

Update requirements file.
Install new package.
Run tests.
Commit to git.
Notify team.

## Upgrading on Notebooks/Colab

```
%pip install pandas~=2.1
```

You might also need to run `Runtime/Restart Session`